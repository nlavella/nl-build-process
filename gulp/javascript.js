const gulp = require("gulp");
const babel = require("gulp-babel");
const eslint = require("gulp-eslint");
const uglify = require("gulp-uglify");
const concat = require("gulp-concat");
const gulpif = require("gulp-if");
const vars = require("./vars");

gulp.task("lint", function () {
	return gulp
		.src([vars.src_directory + "js/**/*.js", vars.src_directory + "js/main.js"])
		.pipe(concat("main.js"))
		.pipe(
			eslint({
				parserOptions: {
					ecmaVersion: 6,
					sourceType: "module",
					ecmaFeatures: {
						jsx: true,
					},
				},
				env: {
					node: true,
					es6: true,
					browser: true,
				},
				extends: "eslint:recommended",
				rules: {
					"no-console": [
						1,
						{
							allow: ["warn", "error"],
						},
					],
					"block-scoped-var": 1,
					eqeqeq: [2, "smart"],
					"no-warning-comments": [
						1,
						{
							terms: ["TODO", "FIXME"],
							location: "start",
						},
					],
					"no-undef-init": 0,
					"no-undef": 0,
					"no-undefined": 0,
					"no-useless-escape": 0,
					"no-unused-vars": 1,
					"arrow-spacing": [
						2,
						{
							before: true,
							after: true,
						},
					],
					"array-bracket-spacing": [0, "never"],
					"block-spacing": [1, "always"],
					"brace-style": [
						1,
						"1tbs",
						{
							allowSingleLine: false,
						},
					],
					camelcase: 1,
					"comma-spacing": [
						1,
						{
							before: false,
							after: true,
						},
					],
					"comma-style": [1, "last"],
					indent: [0, "tab"],
					"no-inline-comments": 0,
					"no-lonely-if": 1,
					"no-mixed-spaces-and-tabs": 1,
					"no-multiple-empty-lines": 1,
					"no-nested-ternary": 1,
					"no-trailing-spaces": 1,
					"space-in-parens": [1, "never"],
				},
			})
		) // see .eslintrc filein theme root for settings
		.pipe(eslint.format())
		.pipe(eslint.failAfterError());
});

gulp.task("babel", function () {
	return (
		gulp
			.src([
				vars.src_directory + "js/**/*.js",
				vars.src_directory + "js/main.js",
			])
			.pipe(concat("main.js"))
			.pipe(
				babel({
					presets: [
						[
							"@babel/preset-env",
							{
								targets: {
									ie: "11",
								},
							},
						],
					],
					plugins: [
						"transform-es2015-template-literals",
						"syntax-dynamic-import",
						"transform-es2015-arrow-functions",
						"transform-es2015-for-of",
						"transform-es2015-parameters",
						[
							"transform-es2015-spread",
							{
								loose: true,
							},
						],
						[
							"transform-es2015-destructuring",
							{
								loose: true,
							},
						],
						[
							"transform-es2015-block-scoping",
							{
								throwIfClosureRequired: false,
							},
						],
					],
					compact: true,
				})
			)
			//.pipe(uglify())
			.pipe(
				gulpif(
					vars.type === "wp",
					gulp.dest(vars.wp_dist_directory + "js"),
					gulp.dest(vars.dist_directory + "js")
				)
			)
			.pipe(browserSync.stream())
	);
});

gulp.task("js", gulp.series("lint", "babel"));
