const gulp = require("gulp");
const gulpif = require("gulp-if");
const vars = require("./vars");

//Copy libs files to dev
gulp.task("libs", function (done) {
  gulp
    .src(vars.nl_directory + "/libs/**")
    .pipe(
      gulpif(
        vars.type === "wp",
        gulp.dest(vars.wp_dist_directory + "libs"),
        gulp.dest(vars.dist_directory + "libs")
      )
    );
  done();
});

//Copy fonts files to dev
gulp.task("fonts", function (done) {
  gulp
    .src(vars.src_directory + "/fonts/**")
    .pipe(
      gulpif(
        vars.type === "wp",
        gulp.dest(vars.wp_dist_directory + "fonts"),
        gulp.dest(vars.dist_directory + "fonts")
      )
    );
  done();
});

//Copy images files to dev
gulp.task("copyImages", function (done) {
  gulp
    .src(vars.src_directory + "/images/**")
    .pipe(
      gulpif(
        vars.type === "wp",
        gulp.dest(vars.wp_dist_directory + "images"),
        gulp.dest(vars.dist_directory + "images")
      )
    );
  done();
});

gulp.task("copyAssets", gulp.series("libs", "fonts", "copyImages"));
