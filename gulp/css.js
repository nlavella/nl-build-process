const gulp = require("gulp");
const cssnano = require("gulp-cssnano");
const sass = require("gulp-sass")(require("sass"));
const sourcemaps = require("gulp-sourcemaps");
const autoprefixer = require("gulp-autoprefixer");
const postcss = require("gulp-postcss");
const vars = require("./vars");

//SCSS Compile
gulp.task("sass", function () {
	return gulp
		.src(vars.src_directory + "scss/main.scss")
		.pipe(sourcemaps.init())
		.pipe(sass().on("error", sass.logError))
		.pipe(cssnano())
		.pipe(sourcemaps.write("."))
		.pipe(gulp.dest(vars.wp_dist_directory + "css"))
		.pipe(browserSync.stream());
});
