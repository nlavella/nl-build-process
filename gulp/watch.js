const gulp = require("gulp");
const vars = require("./vars");

//Watch tasks
gulp.task("dev", function () {
	browserSync.init({
		proxy: "localhost:8000",
		browser: "google chrome",
	});

	// generate
	gulp.watch(vars.src_directory + "scss/**/*.scss", gulp.series("sass"));
	gulp.watch(vars.src_directory + "js/**/*.js", gulp.series("js"));
	gulp.watch(vars.wp_directory + "**/*.php").on("change", browserSync.reload);
});
