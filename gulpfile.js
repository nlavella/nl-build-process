//NL-static-build-process 1.0.0

// Structuring Split Gulp with globalBrowsersync Instance
// https://gist.github.com/larrybotha/a4db8b9d1f8dbf4a196b85437a6c7e0b

const gulp = require("gulp");

// Module to require whole directories
const requireDir = require("require-dir")("./gulp/");

// Add on our Globals
const distPath = "dev/assets/css";
global.browserSync = require("browser-sync").create();
