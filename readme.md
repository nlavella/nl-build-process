## Todo

- remove jquery
- build styleguide builder w/ instructions

- build styleguide/template
- compress dist static
- image sizes gulp

* standardize comment format
* base template
* form processing
* cleanup gulp tasks
* remove jquery functions for vanilla

* sections
  - accordion
  - flex grid
  - img srcset
  - map
  - form
  - modal
  - fancybox
  - animations

## STARTUP

1. Change gulp/vars settings

## WP

1. docker-compose build
2. docker-compose up
3. gulp wpWatch

### 1. `docker-compose build`

### 2. `docker-compose up`

### 3. `docker exec -it <CONTAINER ID FOR REACT-FRONTEND> npm install node-sass`

/bin/bash

** fixes issue with node-sass env **
** might also need `docker exec -it <CONTAINER ID FOR REACT_FRONTEND> npm rebuild node-sass` and restart docker**

## Helpful commands

### `docker container ls`

Show all docker containers running

### `docker kill $ID$`

Kill container

### `docker-compose down`

Reset shared container issue

### WP ADMIN: J8qSrs9xDb
