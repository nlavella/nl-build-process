"use strict";function _typeof(obj){"@babel/helpers - typeof";return _typeof="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(obj){return typeof obj;}:function(obj){return obj&&"function"==typeof Symbol&&obj.constructor===Symbol&&obj!==Symbol.prototype?"symbol":typeof obj;},_typeof(obj);}/*
 * ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 * ========================================================================
 */ // Use this variable to set up the common and page specific functions. If you
// rename this variable, you will also need to rename the namespace below.
var NL={// All pages
common:{init:function init(){// NAV FUNCTION INIT
NL.common.navigation();// Sticky Nav
NL.common.stickyNav();// Scroll links
NL.common.scrollLinks();// Tab Sections
tabSections();// accordion Section
accordionSections();//setup swipers
initSwipers();// Scroll Focus
scrollFocus();// Scroll Animation
// scrollAnimation();
// Parallax Animation
// parallaxAnimation();
// Chase Animation
// chaseAnimation();
// Gallery Sections
//gallerySection();
// Infinite Rotate
// see _infiniteRotate.js
// counting numbers
initCountUp();// SJ Slides
//initSJSlides();
},/*
		 * ========================================================================
		 * Navigation collapse & expand functionality
		 * ========================================================================
		 */navigation:function navigation(){//mobile menu toggle
$(".menu-toggle").click(function(){$("nav").toggleClass("active");$("header").toggleClass("active");});},stickyNav:function stickyNav(){var scrollPos=0;//scroll functions
$(window).scroll(function(){if(window.innerWidth>=960){//960
//add scroll class to header
if($(this).scrollTop()>80){if(document.body.getBoundingClientRect().top>scrollPos){//up
// this refers to window
$("header").addClass("scroll");$("body").addClass("scroll");$(".cta-bar").addClass("active");}else{//down
$("header").removeClass("scroll");$("body").removeClass("scroll");$(".cta-bar").removeClass("active");}}else{$("header").removeClass("scroll");$("body").removeClass("scroll");$(".cta-bar").removeClass("active");}}// saves the new position for iteration.
scrollPos=document.body.getBoundingClientRect().top;});},scrollLinks:function scrollLinks(){$(".scroll-link").click(function(event){$(".scroll-link").removeClass("active");$(this).addClass("active");event.preventDefault();$("html,body").animate({scrollTop:$(this.hash).offset().top-50},1000);if(window.innerWidth<=768){$("nav").removeClass("active");}});}},// page specific start
homepage:{init:function init(){}}// page specific end
};// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var NL_UTIL={fire:function fire(func,funcname,args){var fire;var namespace=NL;funcname=funcname===undefined?"init":funcname;fire=func!=="";fire=fire&&namespace[func];fire=fire&&typeof namespace[func][funcname]==="function";if(fire){namespace[func][funcname](args);}},loadEvents:function loadEvents(){// Fire common init JS
NL_UTIL.fire("common");document.body.className.replace(/-/g,"_").split(/\s+/).forEach(function(classnm){NL_UTIL.fire(classnm);NL_UTIL.fire(classnm,"finalize");});}};// Load Events
NL_UTIL.loadEvents();// accordion section functionality
function accordionSections(){console.log("setup accordions");// grab all tab modules
var accordionSections=document.querySelectorAll("[data-accordion]");var _loop=function _loop(){// solo setting
var _solo=accordionSections[i].dataset.accordion==="solo";console.log(_solo);// grab the items
var accordionItems=accordionSections[i].querySelectorAll("[data-accordion-item]");var _loop2=function _loop2(){var _accordionSection=accordionItems[j];var _accordionHeader=accordionItems[j].querySelector("[data-accordion-header]");_accordionHeader.addEventListener("click",function(){var _isOpen=_accordionSection.classList.contains("open");if(_solo){// clear active tabs
removeClassAll(accordionItems,"open");}else{// clear open tab
_accordionSection.classList.remove("open");}if(!_isOpen){// add active to selected
_accordionSection.classList.add("open");}});};for(j=0;j<accordionItems.length;j++){_loop2();}};for(var i=0;i<accordionSections.length;i++){var j;_loop();}}//check all elements that should activate on scroll
/* example structure */ //<div class="chase" data-chase-speed="650" data-scroll-depth="80"><div class="chase-element" id="#"></div>
function chaseAnimation(){$(".chase").each(function(){//if scrolled into view and not already activated
if(isScrolledIntoView(this,$(this).attr("data-scroll-depth"))&&!$(this).hasClass("active")){//mark as activated
$(this).addClass("active");chase(this,$(this).attr("data-chase-speed"));}});$(window).scroll(function(){$(".chase").each(function(){//if scrolled into view and not already activated
if(isScrolledIntoView(this,$(this).attr("data-scroll-depth"))&&!$(this).hasClass("active")){//mark as activated
$(this).addClass("active");chase(this,$(this).attr("data-chase-speed"));}});});}//chase
function chase(element,duration){var inc=0;$(element).find(".chase-element").each(function(index,element){var chase_element=$(this);setTimeout(function(){chase_element.addClass("active");},duration*inc);inc++;});}function initCountUp(){console.log("setup count up");countThemUp();$(window).scroll(function(){//check all elements that should activate on scrolls
countThemUp();});}function countThemUp(){// grab all numbers
var numbers=document.querySelectorAll("[data-countup]");for(var i=0;i<numbers.length;i++){var dec=0;var decCount=numbers[i].dataset.countup.split(".");if(decCount.length===2){dec=decCount[1].length;}console.log();if(upTo(numbers[i],"module").classList.contains("viewed")&&!numbers[i].classList.contains("active")){var _countup2=new CountUp(numbers[i],0,numbers[i].dataset.countup,dec);_countup2.start();numbers[i].classList.add("active");}}}// Find first ancestor of el with tagName
// or undefined if not found
function upTo(el,tagName){tagName=tagName.toLowerCase();while(el&&el.parentNode){el=el.parentNode;if(el.classList.contains(tagName)){return el;}}// Many DOM methods return null if they don't
// find the element they are searching for
// It would be OK to omit the following and just
// return undefined
return null;}/*

	countUp.js
	by @inorganik

*/ // target = id of html element or var of previously selected html element where counting occurs
// startVal = the value you want to begin at
// endVal = the value you want to arrive at
// decimals = number of decimal places, default 0
// duration = duration of animation in seconds, default 2
// options = optional object of options (see below)
var CountUp=function CountUp(target,startVal,endVal,decimals,duration,options){var self=this;self.version=function(){return"1.9.3";};// default options
self.options={useEasing:true,// toggle easing
useGrouping:true,// 1,000,000 vs 1000000
separator:",",// character to use as a separator
decimal:".",// character to use as a decimal
easingFn:easeOutExpo,// optional custom easing function, default is Robert Penner's easeOutExpo
formattingFn:formatNumber,// optional custom formatting function, default is formatNumber above
prefix:"",// optional text before the result
suffix:"",// optional text after the result
numerals:[]// optionally pass an array of custom numerals for 0-9
};// extend default options with passed options object
if(options&&_typeof(options)==="object"){for(var key in self.options){if(options.hasOwnProperty(key)&&options[key]!==null){self.options[key]=options[key];}}}if(self.options.separator===""){self.options.useGrouping=false;}else{// ensure the separator is a string (formatNumber assumes this)
self.options.separator=""+self.options.separator;}// make sure requestAnimationFrame and cancelAnimationFrame are defined
// polyfill for browsers without native support
// by Opera engineer Erik Möller
var lastTime=0;var vendors=["webkit","moz","ms","o"];for(var x=0;x<vendors.length&&!window.requestAnimationFrame;++x){window.requestAnimationFrame=window[vendors[x]+"RequestAnimationFrame"];window.cancelAnimationFrame=window[vendors[x]+"CancelAnimationFrame"]||window[vendors[x]+"CancelRequestAnimationFrame"];}if(!window.requestAnimationFrame){window.requestAnimationFrame=function(callback,element){var currTime=new Date().getTime();var timeToCall=Math.max(0,16-(currTime-lastTime));var id=window.setTimeout(function(){callback(currTime+timeToCall);},timeToCall);lastTime=currTime+timeToCall;return id;};}if(!window.cancelAnimationFrame){window.cancelAnimationFrame=function(id){clearTimeout(id);};}function formatNumber(num){var neg=num<0,x,x1,x2,x3,i,len;num=Math.abs(num).toFixed(self.decimals);num+="";x=num.split(".");x1=x[0];x2=x.length>1?self.options.decimal+x[1]:"";if(self.options.useGrouping){x3="";for(i=0,len=x1.length;i<len;++i){if(i!==0&&i%3===0){x3=self.options.separator+x3;}x3=x1[len-i-1]+x3;}x1=x3;}// optional numeral substitution
if(self.options.numerals.length){x1=x1.replace(/[0-9]/g,function(w){return self.options.numerals[+w];});x2=x2.replace(/[0-9]/g,function(w){return self.options.numerals[+w];});}return(neg?"-":"")+self.options.prefix+x1+x2+self.options.suffix;}// Robert Penner's easeOutExpo
function easeOutExpo(t,b,c,d){return c*(-Math.pow(2,-10*t/d)+1)*1024/1023+b;}function ensureNumber(n){return typeof n==="number"&&!isNaN(n);}self.initialize=function(){if(self.initialized)return true;self.error="";self.d=typeof target==="string"?document.getElementById(target):target;if(!self.d){self.error="[CountUp] target is null or undefined";return false;}self.startVal=Number(startVal);self.endVal=Number(endVal);// error checks
if(ensureNumber(self.startVal)&&ensureNumber(self.endVal)){self.decimals=Math.max(0,decimals||0);self.dec=Math.pow(10,self.decimals);self.duration=Number(duration)*1000||2000;self.countDown=self.startVal>self.endVal;self.frameVal=self.startVal;self.initialized=true;return true;}else{self.error="[CountUp] startVal ("+startVal+") or endVal ("+endVal+") is not a number";return false;}};// Print value to target
self.printValue=function(value){var result=self.options.formattingFn(value);if(self.d.tagName==="INPUT"){this.d.value=result;}else if(self.d.tagName==="text"||self.d.tagName==="tspan"){this.d.textContent=result;}else{this.d.innerHTML=result;}};self.count=function(timestamp){if(!self.startTime){self.startTime=timestamp;}self.timestamp=timestamp;var progress=timestamp-self.startTime;self.remaining=self.duration-progress;// to ease or not to ease
if(self.options.useEasing){if(self.countDown){self.frameVal=self.startVal-self.options.easingFn(progress,0,self.startVal-self.endVal,self.duration);}else{self.frameVal=self.options.easingFn(progress,self.startVal,self.endVal-self.startVal,self.duration);}}else{if(self.countDown){self.frameVal=self.startVal-(self.startVal-self.endVal)*(progress/self.duration);}else{self.frameVal=self.startVal+(self.endVal-self.startVal)*(progress/self.duration);}}// don't go past endVal since progress can exceed duration in the last frame
if(self.countDown){self.frameVal=self.frameVal<self.endVal?self.endVal:self.frameVal;}else{self.frameVal=self.frameVal>self.endVal?self.endVal:self.frameVal;}// decimal
self.frameVal=Math.round(self.frameVal*self.dec)/self.dec;// format and print value
self.printValue(self.frameVal);// whether to continue
if(progress<self.duration){self.rAF=requestAnimationFrame(self.count);}else{if(self.callback)self.callback();}};// start your animation
self.start=function(callback){if(!self.initialize())return;self.callback=callback;self.rAF=requestAnimationFrame(self.count);};// toggles pause/resume animation
self.pauseResume=function(){if(!self.paused){self.paused=true;cancelAnimationFrame(self.rAF);}else{self.paused=false;delete self.startTime;self.duration=self.remaining;self.startVal=self.frameVal;requestAnimationFrame(self.count);}};// reset to startVal so animation can be run again
self.reset=function(){self.paused=false;delete self.startTime;self.initialized=false;if(self.initialize()){cancelAnimationFrame(self.rAF);self.printValue(self.startVal);}};// pass a new endVal and start animation
self.update=function(newEndVal){if(!self.initialize())return;newEndVal=Number(newEndVal);if(!ensureNumber(newEndVal)){self.error="[CountUp] update() - new endVal is not a number: "+newEndVal;return;}self.error="";if(newEndVal===self.frameVal)return;cancelAnimationFrame(self.rAF);self.paused=false;delete self.startTime;self.startVal=self.frameVal;self.endVal=newEndVal;self.countDown=self.startVal>self.endVal;self.rAF=requestAnimationFrame(self.count);};// format startVal on initialization
if(self.initialize())self.printValue(self.startVal);};var currentFocus;function scrollFocus(){setTimeout(function(){focus();},2000);$(window).scroll(function(){//check all elements that should activate on scrolls
focus();});function focus(){var currentlyAbove=new Array();$(".focus").each(function(){//if scrolled into view and not already activated
if($(this).hasClass("cta_section")&&isScrolledIntoView(this,25)){currentlyAbove.push(this);$(this).addClass("viewed");}else if(passTheHalf(this)){currentlyAbove.push(this);$(this).addClass("viewed");}});if(currentlyAbove[currentlyAbove.length-1]!==currentFocus){//mark as activated
$(".focus").removeClass("inFocus");$(currentlyAbove[currentlyAbove.length-1]).addClass("inFocus");currentFocus=currentlyAbove[currentlyAbove.length-1];//mark nav
$("a.focus-link").each(function(){var currLink=$(this);var refElement=$(currLink.attr("href"));console.log(refElement);console.log(currentFocus);if($(refElement).attr("id")===$(currentFocus).attr("id")){$("a.focus-link").removeClass("active");currLink.addClass("active");}});}}}function bottomSection(elem){$(window).scroll(function(){var viewTop=$(window).scrollTop();var viewBottom=viewTop+$(window).height();var eleTop=$(elem).offset().top;if(eleTop<=viewBottom){//console.log("contact");
currentFocus="contact";$(".focus").removeClass("inFocus");$("a.focus-link").removeClass("active");$("a.focus-link[href$='#contact']").addClass("active");}});}// is an element above percentage
function isScrolledIntoView(elem,percent){//viewport height
var viewTop=$(window).scrollTop();var viewBottom=viewTop+$(window).height()-$(".quote-bar").outerHeight();//% of element is showing to trigger
var elemTrigger=$(elem).offset().top+$(elem).outerHeight()*(percent/100);var amountOverTrigger=elemTrigger-viewBottom;/*
	console.log(elem);
	console.log(elem.getBoundingClientRect());
	console.log($(elem).offset().top + "," + $(elem).outerHeight());
	console.log(elemHalf + "," + viewBottom);
	console.log(viewTop + "," + viewBottom);
	console.log("Amount over marker: " + ());

	console.log($(".quote-bar").outerHeight());

	var pixelsShowing = viewBottom - elem.getBoundingClientRect().top;
	console.log("PIXELS SHOWING: " + pixelsShowing);
	console.log(
		"PERCENT SHOWING: " + pixelsShowing / elem.getBoundingClientRect().height
	);
*/if(window.innerWidth<768){//elemHalf = $(elem).offset().top + 1 * ($(elem).outerHeight() / 2);
}return amountOverTrigger<0;}// has an element passed the half
function passTheHalf(elem){//viewport height
var viewTop=$(window).scrollTop();var viewBottom=viewTop+$(window).height()*0.5;//half of element is showing then return
var eleTop=$(elem).offset().top;//console.log(elem);
//console.log(elemHalf + "," + viewBottom);
return eleTop<=viewBottom;}// Remove class from all elements
function removeClassAll(elements,className){for(var i=0;i<elements.length;i++){elements[i].classList.remove(className);}}// Remove class from all elements by selector
function removeClassAllSelector(selector,className){var elements=document.querySelectorAll(selector);for(var i=0;i<elements.length;i++){elements[i].classList.remove(className);}}// Add class from element by selector
function addClass(selector,className){var element=document.querySelector(selector);element.classList.add(className);}// Remove class from element by selector
function removeClass(selector,className){var element=document.querySelector(selector);element.classList.remove(className);}//check all elements that should activate on scrolls
/* example structure */ //<section class="scroll-animation" data-scroll-depth="70" id="#"></section>
function scrollAnimation(){$(".scroll-animation").each(function(){var depth=50;if($(this).attr("data-scroll-depth")){depth=$(this).attr("data-scroll-depth");}//if scrolled into view and not already activated
if(isScrolledIntoView(this,depth)&&!$(this).hasClass("active")){//mark as activated
$(this).addClass("active");}});$(window).scroll(function(){//check all elements that should activate on scrolls
/* example structure */ //<section class="scroll-animation" data-scroll-depth="70" id="#"></section>
$(".scroll-animation").each(function(){var depth=50;if($(this).attr("data-scroll-depth")){depth=$(this).attr("data-scroll-depth");}//if scrolled into view and not already activated
if(isScrolledIntoView(this,depth)&&!$(this).hasClass("active")){//mark as activated
$(this).addClass("active");}});});}function initSwipers(){console.log("setup sliders");// grab all slider modules
var swiperSections=document.querySelectorAll("[data-swiper]");for(var i=0;i<swiperSections.length;i++){var _swiperSection2=swiperSections[i];if(!_swiperSection2.classList.contains("swiper-cont")){_swiperSection2=swiperSections[i].querySelector(".swiper-cont");}var _swiperPrev2=swiperSections[i].querySelector("[data-swiper-prev]");var _swiperNext2=swiperSections[i].querySelector("[data-swiper-next]");var _swiperPagination2=swiperSections[i].querySelector("[data-swiper-pagination]");var _swiperLoop2=false;if(_swiperSection2.dataset.loop==="true"){_swiperLoop2=true;}var _slidesPerView2=1;var _slidesPerViewTablet2=1;var _spaceBetween2=0;var _spaceBetweenTablet2=0;if(_swiperSection2.dataset.desktopSlides){_slidesPerView2=_swiperSection2.dataset.desktopSlides;_spaceBetween2=30;}if(_swiperSection2.dataset.tabletSlides){_slidesPerViewTablet2=_swiperSection2.dataset.tabletSlides;_spaceBetweenTablet2=15;}//swiper
var _swiper=new Swiper(_swiperSection2,{loop:_swiperLoop2,slidesPerView:_slidesPerView2,spaceBetween:_spaceBetween2,pagination:{el:_swiperPagination2,clickable:true},autoplay:{delay:3500},navigation:{nextEl:_swiperNext2,prevEl:_swiperPrev2},breakpoints:{1400:{slidesPerView:_slidesPerViewTablet2,spaceBetween:_spaceBetweenTablet2},768:{slidesPerView:1,spaceBetween:0}}});/* thumbnail nav 
    swiper.on("slideChange", function () {
      $(".swiper-link").removeClass("active");
      $(
        ".swiper-link[data-slide-target='" + residenceSlider.activeIndex + "']"
      ).addClass("active");
    });

    $(".swiper-link").click(function () {
      _swiper.slideTo($(this).attr("data-slide-target"));
    });
    */ //swipers.push(_swiper);
}}// Tab section functionality
function tabSections(){console.log("setup tabs");// grab all tab modules
var tabSections=document.querySelectorAll("[data-tab-section]");var _loop3=function _loop3(){// grab the links
var tabLinks=tabSections[i].querySelectorAll("[data-tab-target]");// grab the tabs
var tabConts=tabSections[i].querySelectorAll("[data-tab]");var _loop4=function _loop4(){var _tabSection=tabSections[i];tabLinks[j].addEventListener("click",function(){// clear active tabs
removeClassAll(tabLinks,"active");removeClassAll(tabLinks,"mobile-active");removeClassAll(tabConts,"active");removeClassAll(tabConts,"mobile-active");// add active to selected
this.classList.add("active");_tabSection.querySelector("[data-tab='"+this.dataset.tabTarget+"']").classList.add("active");_tabSection.querySelector(".tab-cont").classList.add("active");});};for(j=0;j<tabLinks.length;j++){_loop4();}};for(var i=0;i<tabSections.length;i++){var j;_loop3();}}