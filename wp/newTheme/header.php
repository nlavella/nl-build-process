<?php
/**
 * Header
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<?php echo get_field('head_tracking_codes', 'option'); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/libs/swiper-4.4.2/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/libs/fancybox-3.5.2/jquery.fancybox.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/main.css">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php echo get_field('body_tracking_codes', 'option'); ?>

<header>
	<div class="cont">
		<div class="inner">
			<nav id="main-nav">
				<div class="menu-toggle"><!--Menu--> <span>&#9776;</span></div>
				<?php wp_nav_menu( array( 'theme_location' => 'main-nav' ) );?>
				
				<ul class="social">
					<?php foreach(get_field('social_links', 'option') as $social){ ?>
						<li><a href="<?php echo $social['link']['url']; ?>"><img src="<?php echo $social['icon']; ?>"></a></li>
					<?php } ?>
				</ul>
			</nav>

			<a href="/" class="logo">
				<img src="<?php echo get_field('logo', 'option'); ?>">
			</a>
		</div>
	</div>
</header>

<div class="header-spacing"></div>
<main>