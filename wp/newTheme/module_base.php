<?php 

// Build classes
$moduleClasses = get_row_layout();

// Custom classes
$moduleClasses .= ' '.get_sub_field('classes');
if(get_sub_field('sections')){ $moduleClasses .= ' '.get_sub_field('sections'); }

// Background color
if(get_sub_field('background_color')){ $moduleClasses .= ' '.get_sub_field('background_color').'-bg'; }

//styles
$style = '';
if(get_sub_field('background_image')){ $style = "style='background-image:url(".get_sub_field('background_image').");'"; }
?>

<section class="module focus <?php echo $moduleClasses; ?>" id="<?php the_sub_field('id'); ?>" <?php echo $style; ?>>
    <div class="debug-title"><?php echo get_row_layout(); ?><!-- include some documentation here now --></div>
    
    <?php get_template_part('modules/'.get_row_layout()); ?>

</section>