<?php
/**
 * nlBaseWP functions and definitions
 *
 *
 * @package WordPress
 * @subpackage nlBaseWP
 * @since 1.0
 */

//REMOVE WORDPRESS JUNK
function remove_wordpress_defaults() {
	// category feeds
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	remove_action( 'wp_head', 'feed_links', 2 );
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// Remove oEmbed-specific JavaScript from the front-end and back-end.
	remove_action( 'wp_head', 'wp_oembed_add_host_js' );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
	// Remove oEmbed discovery links.
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
    // Remove the REST API lines from the HTML Header
    remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
    // Remove the REST API endpoint.
    remove_action( 'rest_api_init', 'wp_oembed_register_route' );
    // Turn off oEmbed auto discovery.
    add_filter( 'embed_oembed_discover', '__return_false' );
    // Don't filter oEmbed results.
    remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
    // Remove all embeds rewrite rules.
    // BREAKS PERMALINKS
	//add_filter( 'rewrite_rules_array', 'disable_embeds_rewrites' );
	// Remove Emoji support
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
	remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	// remove WP 4.9+ dns-prefetch nonsense
	remove_action( 'wp_head', 'wp_resource_hints', 2 );
}

add_action( 'after_setup_theme', 'remove_wordpress_defaults' );

// wp menus
add_theme_support( 'menus' );

// register header and footer menu option
register_nav_menus(
	array(
		'main-nav' => __( 'Main Menu'),   // main nav
	)
);

//options page for ACF
if( function_exists('acf_add_options_page') ) {	
	acf_add_options_page( array(
 
		'page_title' => 'Global',
		'menu_title' => 'Global',
		'menu_slug' => 'global-fields',
		'capability' => 'edit_posts',
		'icon_url' => 'dashicons-admin-site', // Add this line and replace the second inverted commas with class of the icon you like
		'position' => 2
		 
		));
}

/*
	* Let WordPress manage the document title.
	* By adding theme support, we declare that this theme does not use a
	* hard-coded <title> tag in the document head, and expect WordPress to
	* provide it for us.
	*/
add_theme_support( 'title-tag' );

/**
 * Hide editor
*/
add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
	// Get the Post ID.
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	if( !isset( $post_id ) ) return;

	// Hide editor from all but blog posts
	remove_post_type_support('page', 'editor');
	remove_post_type_support('post', 'editor');
	
}


//excerpt
function new_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

function custom_excerpt_length( $length ) {
	return 15;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/* // custom post type
//Model post type
function model_custom_post_type(){
	register_post_type('Models',
		[
			'labels' => [
				'name' 			=> __('Models'),
                'singular_name' => __('Model'),
			],
			'menu_position' => 4,//4+ to come after posts
			'menu_icon'		=> 'dashicons-admin-home',//c
            'public'     	=> true,
            'has_archive' 	=> false,
			'publicly_queryable'  => false,
			'rewrite'     	=> ['slug' => 'model', 'with_front' => false], // my custom slug
		]
    );
}
add_action('init', 'model_custom_post_type');
*/