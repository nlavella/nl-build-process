<?php
/**
 * Archive
**/

get_header(); ?>

<div class="full blog-posts">
	<div class="cont">
		<?php if ( have_posts() ) : ?>

			<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();
			?>
				<a href="<?php echo the_permalink();?>"><?php echo the_title(); ?></a>
			<?php 
			// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
				'next_text'          => __( 'Next page', 'twentyfifteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
			) );

		endif;
		?>
	</div>
</div>

<?php get_footer(); ?>
