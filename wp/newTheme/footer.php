<?php
/**
 * Footer
 *
 */

?>

</main>

<footer>
  <div class="cont">
    <p><?php the_field('copyright', 'options'); ?></p>
  </div>
</footer>

<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>

<script src="<?php bloginfo('template_url'); ?>/assets/libs/swiper-4.4.2/js/swiper.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/libs/fancybox-3.5.2/jquery.fancybox.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/js/main.js"></script>

<?php echo get_field('footer_tracking_codes', 'option'); ?> 
<?php wp_footer(); ?>
</body>
</html>
