<div class="full">
        <div class="cont">
                <?php if(get_sub_field('eyebrow')){ ?><p class="eyebrow"><?php the_sub_field('eyebrow'); ?></p><?php } ?>
                <?php if(get_sub_field('headline')){ ?><h2><?php the_sub_field('headline'); ?></h2><?php } ?>
                <?php if(get_sub_field('subheadline')){ ?><p class="title"><?php the_sub_field('subheadline'); ?></p><?php } ?>
                <?php if(get_sub_field('body')){ ?> <div class="desc"><?php the_sub_field('body'); ?></div><?php } ?>

        <?php if(get_sub_field('links')){ ?>
                <ul class="links">
                <?php foreach(get_sub_field('links') as $section){ ?>
                <li>
                        <?php if($section['link']){ 
                                $link = $section['link'];
                        ?>
                                <a href="<?php echo $link['url'];?>">
                        <?php } ?>
                                <?php if($section['image']){
                                        $responsive_image = $section['image'];
                                        echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                } ?>
                                <div class="inner">
                                        <?php if($section['headline']){ ?><h2><?php echo $section['headline']; ?></h2><?php } ?>
                                        <?php if($section['body']){ ?> <div class="desc"><?php echo $section['body']; ?></div><?php } ?>
                                </div>
                        <?php if($section['link']){ ?>
                                </a>
                        <?php } ?>
                </li>
                <?php } ?>
                </ul>
        <?php } ?>
        </div>
</div>