<div class="full">
        <div class="cont">
                <?php if(get_sub_field('headline')){ ?><h2><?php the_sub_field('headline'); ?></h2><?php } ?>
                <?php if(get_sub_field('subheadline')){ ?><p class="title"><?php the_sub_field('subheadline'); ?></p><?php } ?>
        </div>
</div>

<?php if(get_sub_field('general_sections')){ ?>
        <?php foreach(get_sub_field('general_sections') as $section){ ?>
            <div class="general_section">
                <div class="cont">
                        <?php if($section['image']){
                                $responsive_image = $section['image'];
                                echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                        } ?>
                        <?php if($section['headline']){ ?><h2><?php echo $section['headline']; ?></h2><?php } ?>
                        <?php if($section['body']){ ?> <div class="desc"><?php echo $section['body']; ?></div><?php } ?>
                        <?php if($section['link']){ 
                                $link = $section['link'];
                        ?>
                                <a href="<?php echo $link['url'];?>" class="btn"><?php echo $link['title'];?></a>
                        <?php } ?>
                </div> 
            </div>
        <?php } ?>
<?php } ?>