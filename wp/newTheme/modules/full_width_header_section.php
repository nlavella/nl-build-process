<div class="cont">
            <?php 
                $brand = get_field('brand');
                if($brand){ ?>
                    <?php if(get_field('logo', $brand[0]->ID)){
                        $responsive_image = get_field('logo', $brand[0]->ID);
                        echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => 'logo', 'alt' => $responsive_image['alt'] )); 
                    } ?>
            <?php } ?>
            <?php if(get_sub_field('image')){ ?>
                <div class="img-crop sixteen-nine">
                    <?php 
                        $responsive_image = get_sub_field('image');
                        echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                        ?>
                </div>
            <?php } ?>
            <div class="col">
                <h1><?php the_sub_field('headline'); ?></h1>
                <?php if(get_sub_field('body')){ ?> <div class="desc"><?php the_sub_field('body'); ?></div><?php } ?>
            </div>
        </div>
</div>