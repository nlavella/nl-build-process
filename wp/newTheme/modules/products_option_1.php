<?php if(get_sub_field('eyebrow')){ ?><div class="eyebrow callout"><?php the_sub_field('eyebrow'); ?><div class="triangle"></div></div> <?php } ?>
<div class="cont">
    <?php foreach(get_sub_field('categories') as $category){ ?>
        <?php if($category['headline']){ ?><h2 class="title1 t-center"><?php echo $category['headline']; ?></h2><?php } ?>
        <?php foreach($category['products'] as $product){ ?>
            <div class="column-layout">
                <?php if($product['images']){ ?>
                <div class="col">
                    <?php if(count($product['images']) == 1){ 
                        foreach($product['images'] as $image){ ?>
                            <div class="img-crop square">
                                <?php 
                                    $responsive_image = $image;
                                    echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                ?>
                            </div>
                    <?php }
                        }else{ ?>
                        <div class="swiper-section btm-btns" data-swiper>
                            <div class="swiper-cont">
                                <div class="swiper-wrapper">
                                <?php foreach($product['images'] as $image){ ?>
                                    <div class="swiper-slide">
                                        <div class="img-crop square">
                                            <?php 
                                                $responsive_image = $image;
                                                echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                            ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                </div><!-- /swiper-wrapper -->
                            </div><!-- /swiper-cont -->
                            <div class="swiper-button-prev" data-swiper-prev></div>
                            <div class="swiper-button-next" data-swiper-next></div>
                        </div><!-- /swiper-section -->
                    <?php 
                        } ?>
                </div>
                <?php } ?>
                <div class="col">
                    <?php if($product['product_name']){ ?><p class="title3"><?php echo $product['product_name']; ?></p><?php } ?>
                    <?php if($product['description']){ ?> <div class="desc"><?php echo $product['description']; ?></div><?php } ?>
                    <?php 
                        $button = $product['button'];
                        if($button['pdf_or_link'] == "pdf" && $button['file']){ ?>
                        <a href="<?php echo $button['file']['url']; ?>" class="btn" target="_blank"><?php echo $button['title']; ?></a>
                    <?php }else if($button['link']){ ?>
                        <a href="<?php echo $button['link']['url']; ?>" class="btn" target="<?php echo $button['link']['target']; ?>"><?php echo $button['link']['title']; ?></a>
                    <?php } ?>
                </div>     
            </div>
        <?php } ?> 
    <?php } ?>    
</div>
