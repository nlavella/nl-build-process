<div class="cont">
        <?php if(get_sub_field('image')){
                $responsive_image = get_sub_field('image');
                echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
        } ?>
        <?php if(get_sub_field('eyebrow')){ ?><p class="eyebrow"><?php the_sub_field('eyebrow'); ?></p><?php } ?>
        <?php if(get_sub_field('headline')){ ?><h2><?php the_sub_field('headline'); ?></h2><?php } ?>
        <?php if(get_sub_field('subheadline')){ ?><p class="title"><?php the_sub_field('subheadline'); ?></p><?php } ?>
        <?php if(get_sub_field('body')){ ?> <div class="desc"><?php the_sub_field('body'); ?></div><?php } ?>
        <?php if(get_sub_field('link')){ 
                $link = get_sub_field('link');
        ?>
                <a href="<?php echo $link['url'];?>" class="btn"><?php echo $link['title'];?></a>
        <?php } ?>
</div>