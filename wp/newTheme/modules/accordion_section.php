<div class="cont">  
        <div class="accordion-section" data-accordion>
                <div class="accordion-item" data-accordion-item>
                        <div class="accordion-header" data-accordion-header>
                                <div class="accordion-icon">
                                        <span data-accordion-open>+</span>
                                        <span data-accordion-close>-</span>
                                </div>
                                <p><?php the_sub_field('title'); ?></p>
                        </div>
                        <div class="accordion-details">
                                <?php the_sub_field('description'); ?>
                        </div>
                </div>
        </div>
</div>