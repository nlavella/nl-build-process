<div class="full">
        <div class="cont">
                <?php if(get_sub_field('headline')){ ?><h2><?php the_sub_field('headline'); ?></h2><?php } ?>
        </div>
</div>

<?php if(get_sub_field('slides')){ ?>
<div class="swiper-section">
    <div class="swiper-cont" data-swiper data-desktop-slides="4">
        <div class="swiper-wrapper">
        <?php foreach(get_sub_field('slides') as $slide){ ?>
            <div class="swiper-slide">
                <div class="general_section">
                        <div class="cont">
                                <?php if($slide['image']){
                                        $responsive_image = $slide['image'];
                                        echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                } ?>
                                <?php if($slide['headline']){ ?><h2><?php echo $slide['headline']; ?></h2><?php } ?>
                                <?php if($slide['body']){ ?> <div class="desc"><?php echo $slide['body']; ?></div><?php } ?>
                                <?php if($slide['link']){ 
                                        $link = $slide['link'];
                                ?>
                                        <a href="<?php echo $link['url'];?>" class="btn"><?php echo $link['title'];?></a>
                                <?php } ?>
                        </div>
                </div>
            </div>
        <?php } ?>
        </div><!-- /swiper-wrapper -->
        <div class="swiper-button-prev" data-swiper-prev></div>
	<div class="swiper-button-next" data-swiper-next></div>
        <div class="swiper-pagination" data-swiper-pagination></div>
    </div>
</div>
<?php } ?>