<?php if(get_sub_field('eyebrow')){ ?><div class="eyebrow callout"><?php the_sub_field('eyebrow'); ?><div class="triangle"></div></div> <?php } ?>

<div class="btm">
    <div class="cont">
        <div class="card-layout halves">
            <?php foreach(get_sub_field('products') as $product){ 
                if($product['layout'] == 'half'){ ?>
                    <div class="card half">
                        <p class="title3"><?php echo $product['product_name']; ?></p>
                        <div class="img-cont">
                            <div class="img-crop square">
                                <?php 
                                    $responsive_image = $product['image'];
                                    echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                ?>
                            </div>
                        </div>
                        <div class="flex-center">
                            <?php 
                                $button = $product['button'];
                                if($button['pdf_or_link'] == "pdf" && $button['file']){ ?>
                                <a href="<?php echo $button['file']['url']; ?>" class="btn secondary" target="_blank"><?php echo $button['title']; ?></a>
                            <?php }else if($button['link']){ ?>
                                <a href="<?php echo $button['link']['url']; ?>" class="btn secondary" target="<?php echo $button['link']['target']; ?>"><?php echo $button['link']['title']; ?></a>
                            <?php } ?>
                            <?php if($product['video']){ ?>
                                <a href="<?php echo $product['video']; ?>" class="btn video" data-fancybox>Play Video</a>
                            <?php }?>
                        </div>
                    </div>
                <?php }else if($product['layout'] == 'full'){ ?>
                    <div class="card full">
                        <div class="column-layout">
                            <div class="col">
                                <div class="img-cont">
                                    <div class="img-crop square">
                                    <?php 
                                        $responsive_image = $product['image'];
                                        echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                    ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <p class="title3"><?php echo $product['product_name']; ?></p>
                            </div>
                            <div class="col">
                                <div class="img-cont">
                                    <div class="img-crop square">
                                    <?php 
                                        $responsive_image = $product['second_image'];
                                        echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                    ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-center">
                             <?php 
                                $button = $product['button'];
                                if($button['pdf_or_link'] == "pdf" && $button['file']){ ?>
                                <a href="<?php echo $button['file']['url']; ?>" class="btn secondary" target="_blank"><?php echo $button['title']; ?></a>
                            <?php }else if($button['link']){ ?>
                                <a href="<?php echo $button['link']['url']; ?>" class="btn secondary" target="<?php echo $button['link']['target']; ?>"><?php echo $button['link']['title']; ?></a>
                            <?php } ?>
                            <?php if($product['video']){ ?>
                                <a href="<?php echo $product['video']; ?>" class="btn video" data-fancybox>Play Video</a>
                            <?php }?>
                        </div>
                    </div>
                <?php }else if($product['layout'] == 'full-bg'){ ?>
                    <div class="card full full-bg" style="background-image:url(<?php echo $product['image']['url']; ?>);">
                        <div class="flex-center">
                            <h2><?php echo $product['product_name']; ?></h2>
                        </div>
                        <div class="flex-center">
                            <?php 
                                $button = $product['button'];
                                if($button['pdf_or_link'] == "pdf" && $button['file']){ ?>
                                <a href="<?php echo $button['file']['url']; ?>" class="btn secondary" target="_blank"><?php echo $button['title']; ?></a>
                            <?php }else if($button['link']){ ?>
                                <a href="<?php echo $button['link']['url']; ?>" class="btn secondary" target="<?php echo $button['link']['target']; ?>"><?php echo $button['link']['title']; ?></a>
                            <?php } ?>
                            <?php if($product['video']){ ?>
                                <a href="<?php echo $product['video']; ?>" class="btn video" data-fancybox>Play Video</a>
                            <?php }?>
                        </div>
                    </div>
                <?php }
            } ?>
        </div>
    </div>
</div>