<script>
	var mapStyles=[{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}];

	var map;
	var poiMarkers = [];
	var mainMarker;
	
	// Map points
	var mapPoints = [
			<?php 
				
				$locations = get_sub_field('locations');
				$count = 0;
				
				foreach($locations as $location){ 
					echo '{"id":'.$count.', "url":"", "address": "'.$location['address'].'", "title": "'.$location['title'].'", "lat":'.$location['latitude'].', "lng":'.$location['longitude'].'},';
					
					$count ++;
				} 
			?>
		];

	// Enable custom icon
	var icon = {
		url: "<?php bloginfo('template_url'); ?>/assets/images/pin.svg",
		scaledSize: new window.google.maps.Size(30, 45),
	};
	
	function initMap() {
		const theMap = document.querySelector("[data-map]");

		//Initalize google map
		let map = new google.maps.Map(theMap, {
			zoom: 15, //5 - contient, 10 - city, 15 - streets, 20 - buildings
			styles: mapStyles,
			center: new google.maps.LatLng(39.940768, -75.15065),
		});

		var mainMarker = new google.maps.Marker({
			position: new google.maps.LatLng(39.940768, -75.15065),
			map: map,
			icon: icon,
			info: "Immerge Interactive",
		});

		google.maps.event.addListener(mainMarker, 'click', function(){
			infoWindow.setContent(this.info);
            infoWindow.open(map, this);
        });
		
		 //POI markers
		 for(var i=0; i<mapPoints.length; i++){
			
			poiMarkers[i] = new google.maps.Marker({
				position:  new google.maps.LatLng(mapPoints[i].lat, mapPoints[i].lng),
				map: map,
				icon: icon,
				info: '<a href="'+mapPoints[i].url+'"><strong>'+mapPoints[i].title+'</strong></a>',
				title: mapPoints[i].title,
			});

			var infoWindow = new google.maps.InfoWindow({content:'<a href="'+mapPoints[i].url+'"><strong>'+mapPoints[i].title+'</strong></a>'});

			google.maps.event.addListener(poiMarkers[i], 'click', function(){
				infoWindow.setContent(this.info);
				infoWindow.open(map, this);
			});
		}
							
		//Center & resize	
		var bounds = new google.maps.LatLngBounds();
		for(var i=0; i<poiMarkers.length; i++){
			bounds.extend(poiMarkers[i].getPosition());
		}
		map.fitBounds(bounds);
						
		google.maps.event.addDomListener(window, 'resize', function(){
			var center = map.getCenter();
			google.maps.event.trigger(map, "resize");
			map.setCenter(center);
		});

	}

</script>

<div class="cont">
    <?php if(get_sub_field('headline')){ ?><h2><?php the_sub_field('headline'); ?></h2><?php } ?>
    <?php if(get_sub_field('subheadline')){ ?><p><?php the_sub_field('subheadline'); ?></p><?php } ?>
	<div class="the-map" data-map></div>
</div>


<!-- REPLACE API KEY ******** -->
<script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCy6cAjuby6bfdTdN2DPjb7H4aK6bJuvd8&callback=initMap">
</script>