<div class="cont">
        <?php if(get_sub_field('headline')){ ?><h2><?php the_sub_field('headline'); ?></h2><?php } ?>
        <?php if(get_sub_field('subheadline')){ ?><p class="title"><?php the_sub_field('subheadline'); ?></p><?php } ?>    
        
        <?php
        $table = get_sub_field( 'table' );

        if ( ! empty ( $table ) ) {

        echo '<table border="0">';

                if ( ! empty( $table['header'] ) ) {

                echo '<thead>';

                        echo '<tr>';

                        foreach ( $table['header'] as $th ) {

                                echo '<th>';
                                echo $th['c'];
                                echo '</th>';
                        }

                        echo '</tr>';

                echo '</thead>';
                }

                echo '<tbody>';

                foreach ( $table['body'] as $tr ) {

                        echo '<tr>';

                        foreach ( $tr as $td ) {

                                echo '<td>';
                                echo $td['c'];
                                echo '</td>';
                        }

                        echo '</tr>';
                }

                echo '</tbody>';

        echo '</table>';
        } ?>
</div>