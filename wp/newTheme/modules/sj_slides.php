<?php if(get_sub_field('slide')){ ?>
        <div class="sj-outer" style="height: <?php echo (sizeof(get_sub_field('slide')))*150; ?>vh" data-sjslides-section>
                <div class="sj-inner">
                        <?php $count = 0; foreach(get_sub_field('slide') as $section){ ?>
                        <div class="general_section slide" id="sj-slide-<?php echo $count;?>" data-slide>
                                <div class="cont">
                                        <?php if($section['image']){
                                                $responsive_image = $section['image'];
                                                echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                        } ?>
                                        <?php if($section['headline']){ ?><h2><?php echo $section['headline']; ?></h2><?php } ?>
                                        <?php if($section['body']){ ?> <div class="desc"><?php echo $section['body']; ?></div><?php } ?>
                                        <?php if($section['link']){ 
                                                $link = $section['link'];
                                        ?>
                                                <a href="<?php echo $link['url'];?>" class="btn"><?php echo $link['title'];?></a>
                                        <?php } ?>
                                </div> 
                        </div>
                        <?php $count ++; } ?>
                </div>
        </div>
<?php } ?>