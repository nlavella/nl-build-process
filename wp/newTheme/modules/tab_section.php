<div class="cont">
    <?php if(get_sub_field('headline')){ ?><h2><span><?php the_sub_field('eyebrow'); ?></span><?php the_sub_field('headline'); ?></h2><?php } ?>

    <div class="tab-section" data-tab-section>
        <div class="tab-nav">
        <?php 
            $count = 0;
            if(get_sub_field('tabs')){

                foreach(get_sub_field('tabs') as $tab){ ?>

                <div class="tab-link <?php if($count == 0){ echo 'active'; }?>" data-tab-target="<?php echo $count; ?>"><?php echo $tab['title']; ?></div>

        <?php   
                $count ++;
                }
            }
        ?>
        </div>

        <div class="tab-cont">
        <?php 
            $count = 0;
            if(get_sub_field('tabs')){

                foreach(get_sub_field('tabs') as $tab){

                echo '<div class="tab-item ';
                if($count == 0){ echo 'active'; } 
                echo '" data-tab="'.$count.'">';

                    if($tab['image']){
                        echo '<img src="'.$tab['image']['url'].'" />';
                    }

                    echo '<div class="inner">'.$tab['body'].'</div>';

                echo '</div>';
                $count ++;
                }
            }
        ?>
        </div>
    </div>
</div>