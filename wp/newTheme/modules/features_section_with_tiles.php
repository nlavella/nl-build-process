<?php if(get_sub_field('eyebrow')){ ?><div class="eyebrow callout"><?php the_sub_field('eyebrow'); ?><div class="triangle"></div></div> <?php } ?>
<div class="cont">
    <div class="col">
        <?php if(get_sub_field('headline')){ ?><h2><?php the_sub_field('headline'); ?></h2><?php } ?>
        <?php if(get_sub_field('body')){ ?> <div class="desc"><?php the_sub_field('body'); ?></div><?php } ?>
    </div>
</div>
<div class="cont">              
    <?php if(get_sub_field('tiles')){ ?>
    <?php if(count(get_sub_field('tiles')) == 1){ ?>
        <div class="card-layout quarters">
            <?php foreach(get_sub_field('tiles') as $slide){ ?>
                <div class="swiper-slide card">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <div class="img-crop square">
                                    <?php if($slide['image']){
                                        $responsive_image = $slide['image'];
                                        echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                    } ?>  
                                </div>
                            </div>
                            <div class="flip-card-back">
                                <div class="inner">
                                    <?php if($slide['headline']){?><p class="title4"><?php echo $slide['headline']; ?></p><?php } ?>
                                    <?php echo $slide['body']; ?>
                                    <?php 
                                        $button = $slide['link'];
                                        if($button['pdf_or_link'] == "pdf" && $button['file']){ ?>
                                        <a href="<?php echo $button['file']['url']; ?>" class="btn" target="_blank"><img src="<?php bloginfo('template_url'); ?>/assets/images/left-arrow.png" /></a>
                                    <?php }else if($button['link']){ ?>
                                        <a href="<?php echo $button['link']['url']; ?>" class="btn" target="<?php echo $button['link']['target']; ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/left-arrow.png" /></a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php }else if(count(get_sub_field('tiles')) < 5){ ?>
        <div class="card-layout quarters desktop">
            <?php foreach(get_sub_field('tiles') as $slide){ ?>
                <div class="swiper-slide card">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <div class="img-crop square">
                                    <?php if($slide['image']){
                                        $responsive_image = $slide['image'];
                                        echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                    } ?>  
                                </div>
                            </div>
                            <div class="flip-card-back">
                                <div class="inner">
                                    <?php if($slide['headline']){?><p class="title4"><?php echo $slide['headline']; ?></p><?php } ?>
                                    <?php echo $slide['body']; ?>
                                    <?php 
                                        $button = $slide['link'];
                                        if($button['pdf_or_link'] == "pdf" && $button['file']){ ?>
                                        <a href="<?php echo $button['file']['url']; ?>" class="btn" target="_blank"><img src="<?php bloginfo('template_url'); ?>/assets/images/left-arrow.png" /></a>
                                    <?php }else if($button['link']){ ?>
                                        <a href="<?php echo $button['link']['url']; ?>" class="btn" target="<?php echo $button['link']['target']; ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/left-arrow.png" /></a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="swiper-section inset mobile" data-swiper>
        <div class="swiper-cont" data-desktop-slides="4">
            <div class="swiper-wrapper">
            <?php foreach(get_sub_field('tiles') as $slide){ ?>
                <div class="swiper-slide">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <div class="img-crop square">
                                    <?php if($slide['image']){
                                        $responsive_image = $slide['image'];
                                        echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                    } ?>  
                                </div>
                            </div>
                            <div class="flip-card-back">
                                <div class="inner">
                                    <?php if($slide['headline']){?><p class="title4"><?php echo $slide['headline']; ?></p><?php } ?>
                                    <?php echo $slide['body']; ?>
                                    <?php 
                                        $button = $slide['link'];
                                        if($button['pdf_or_link'] == "pdf" && $button['file']){ ?>
                                        <a href="<?php echo $button['file']['url']; ?>" class="btn" target="_blank"><img src="<?php bloginfo('template_url'); ?>/assets/images/left-arrow.png" /></a>
                                    <?php }else if($button['link']){ ?>
                                        <a href="<?php echo $button['link']['url']; ?>" class="btn" target="<?php echo $button['link']['target']; ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/left-arrow.png" /></a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            </div><!-- /swiper-wrapper -->
        </div><!-- /swiper-cont -->
        <div class="swiper-button-prev" data-swiper-prev></div>
        <div class="swiper-button-next" data-swiper-next></div>
    </div><!-- /swiper-section -->
    <?php }else{ ?>
        <div class="swiper-section inset" data-swiper>
            <div class="swiper-cont" data-desktop-slides="4">
                <div class="swiper-wrapper">
                <?php foreach(get_sub_field('tiles') as $slide){ ?>
                    <div class="swiper-slide">
                        <div class="flip-card">
                            <div class="flip-card-inner">
                                <div class="flip-card-front">
                                    <div class="img-crop square">
                                        <?php if($slide['image']){
                                            $responsive_image = $slide['image'];
                                            echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                        } ?>  
                                    </div>
                                </div>
                                <div class="flip-card-back">
                                    <div class="inner">
                                        <?php if($slide['headline']){?><p class="title4"><?php echo $slide['headline']; ?></p><?php } ?>
                                        <?php echo $slide['body']; ?>
                                        <?php 
                                            $button = $slide['link'];
                                            if($button['pdf_or_link'] == "pdf" && $button['file']){ ?>
                                            <a href="<?php echo $button['file']['url']; ?>" class="btn" target="_blank"><img src="<?php bloginfo('template_url'); ?>/assets/images/left-arrow.png" /></a>
                                        <?php }else if($button['link']){ ?>
                                            <a href="<?php echo $button['link']['url']; ?>" class="btn" target="<?php echo $button['link']['target']; ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/left-arrow.png" /></a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div><!-- /swiper-wrapper -->
            </div><!-- /swiper-cont -->
            <div class="swiper-button-prev" data-swiper-prev></div>
            <div class="swiper-button-next" data-swiper-next></div>
        </div><!-- /swiper-section -->
    <?php } ?>
    <?php } ?>
</div>