<div class="cont">  
        <?php if(get_sub_field('headline')){ ?><h2 class="title1"><?php the_sub_field('headline'); ?></h2><?php } ?>

        <?php 
             if(get_sub_field('images_or_video') === 'video'){ ?>
                <div class="responsive-video">
                        <div class="video-size-frame"><?php the_sub_field('video_embed_code'); ?></div>
                </div>
        <?php }else{ ?>
                <?php if(count(get_sub_field('images')) == 1){ 
                        foreach(get_sub_field('images')  as $image){ ?>
                            <div class="img-crop sixteen-nine">
                                <?php 
                                    $responsive_image = $image;
                                    echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                ?>
                            </div>
                    <?php }
                        }else{ ?>
                        <div class="swiper-section inset btm-btns" data-swiper>
                            <div class="swiper-cont">
                                <div class="swiper-wrapper">
                                <?php foreach(get_sub_field('images') as $image){ ?>
                                    <div class="swiper-slide">
                                        <div class="img-crop sixteen-nine">
                                            <?php 
                                                $responsive_image = $image;
                                                echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                            ?>
                                        </div>
                                        <?php if($image['caption']){ ?>
                                            <p><?php echo $image['caption']; ?></p>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                                </div><!-- /swiper-wrapper -->
                            </div><!-- /swiper-cont -->
                            <div class="swiper-button-prev" data-swiper-prev></div>
                            <div class="swiper-button-next" data-swiper-next></div>
                        </div><!-- /swiper-section -->
                    <?php 
                        } ?>
        <?php } ?>
</div>