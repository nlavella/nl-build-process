<?php if(get_sub_field('headline')){ ?><h2 class="sectionTitle"><?php the_sub_field('headline'); ?></h2><?php } ?>
<?php if(get_sub_field('slides')){ ?>
<div class="swiper-section">
    <div class="swiper-cont" data-swiper>
        <div class="swiper-wrapper">
        <?php foreach(get_sub_field('slides') as $slide){ ?>
            <div class="swiper-slide" style="background-image:url(<?php echo $slide['image']; ?>)">
                <div class="cont">
                    <div class="flex-center">
                        <div class="col">
                            <?php if($slide['quote']){ ?><p class="title4"><?php echo $slide['quote']; ?></p><?php } ?>
                        </div>
                    </div>
                </div> 
            </div>
        <?php } ?>
        </div><!-- /swiper-wrapper -->
        <div class="swiper-button-prev" data-swiper-prev></div>
		<div class="swiper-button-next" data-swiper-next></div>
    </div>
</div>
<?php } ?>