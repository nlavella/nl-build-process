<div class="cont">
        <div class="column-layout">
                <?php if(get_sub_field('image')){ ?>
                <div class="col">
                <div class="img-crop sixteen-nine">
                        <?php 
                        $responsive_image = get_sub_field('image');
                        echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                        ?>
                </div>
                </div>
                <?php } ?>
                <div class="col">      
                        <?php if(get_sub_field('headline')){ ?><h2 class="title1"><?php the_sub_field('headline'); ?></h2><?php } ?>
                        <div class="accordion-section" data-accordion="solo">
                                <?php foreach(get_sub_field('accordions') as $acc){ ?>
                                <div class="accordion-item" data-accordion-item>
                                        <div class="accordion-header" data-accordion-header>
                                                <p><?php echo $acc['title']; ?></p>
                                                <div class="accordion-icon"></div>
                                        </div>
                                        <div class="accordion-details">
                                                <?php echo $acc['description']; ?>
                                        </div>
                                </div>
                                <?php } ?>
                        </div>
                </div>
        </div>
</div>