<div class="cont">
    <div class="column-layout">
        <?php if(get_sub_field('image')){ ?>
        <div class="col">
            <div class="img-crop square">
                <?php 
                    $responsive_image = get_sub_field('image');
                    echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                ?>
            </div>
        </div>
        <?php } ?>
        <div class="col">
            <?php if(get_sub_field('headline')){ ?><h2 class="title1"><?php the_sub_field('headline'); ?></h2><?php } ?>
            <?php if(get_sub_field('body')){ ?> <div class="desc"><?php the_sub_field('body'); ?></div><?php } ?>
            <div class="flex links">
                <?php 
                    $button = get_sub_field('button'); 
                    if($button['pdf_or_link'] == "pdf" && $button['file']){ ?>
                    <a href="<?php echo $button['file']['url']; ?>" class="btn" target="_blank"><?php echo $button['title']; ?></a>
                <?php }else if($button['link']){ ?>
                    <a href="<?php echo $button['link']['url']; ?>" class="btn" target="<?php echo $button['link']['target']; ?>"><?php echo $button['link']['title']; ?></a>
                <?php } ?>
                <?php if(get_sub_field('video_link')){ ?>
                    <a href="<?php the_sub_field('video_link'); ?>" class="video-btn" data-fancybox>Watch Video<img src="<?php bloginfo('template_url'); ?>/assets/images/blue-play.png" /></a>
                <?php }?>
            </div>
        </div>     
    </div>
</div>