<?php if(get_sub_field('slides')){ ?>
<div class="swiper-section">
    <div class="swiper-cont light" data-swiper>
        <div class="swiper-wrapper">
        <?php foreach(get_sub_field('slides') as $slide){ ?>
            <div class="swiper-slide" style="background-image:url(<?php echo $slide['background_image']['url']; ?>)">
                <div class="cont">
                    <?php if($slide['headline']){ ?><h1><?php echo $slide['headline']; ?></h1><?php } ?>  
                </div> 
            </div>
        <?php } ?>
        </div><!-- /swiper-wrapper -->
        <div class="swiper-button-prev" data-swiper-prev></div>
		<div class="swiper-button-next" data-swiper-next></div>
        <div class="swiper-pagination" data-swiper-pagination></div>
    </div>
</div>
<div class="swiper-section inset btm-btns" data-swiper>
    <div class="swiper-cont">
        <div class="swiper-wrapper">
            <?php foreach(get_sub_field('images') as $image){ ?>
                <div class="swiper-slide">
                    <div class="img-crop sixteen-nine">
                        <?php 
                            $responsive_image = $image;
                            echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                        ?>
                    </div>
                    <?php if($image['caption']){ ?>
                        <p><?php echo $image['caption']; ?></p>
                    <?php } ?>
                </div>
            <?php } ?>
        </div><!-- /swiper-wrapper -->
    </div><!-- /swiper-cont -->
    <div class="swiper-button-prev" data-swiper-prev></div>
    <div class="swiper-button-next" data-swiper-next></div>
</div><!-- /swiper-section -->
<?php } ?>