<?php if(get_sub_field('image')){
        $responsive_image = get_sub_field('image');
        echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => 'full', 'alt' => $responsive_image['alt'] )); 
} ?>
<div class="cont">
        <div class="col">
                <?php if(get_sub_field('headline')){ ?><h2 class="title1"><?php the_sub_field('headline'); ?></h2><?php } ?>
                <?php if(get_sub_field('body')){ ?> <div class="desc"><?php the_sub_field('body'); ?></div><?php } ?>
        </div>
</div>