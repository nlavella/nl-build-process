<div class="cont">
    <div class="flex-center">
        <?php if(get_sub_field('headline')){ ?><h2><?php the_sub_field('headline'); ?></h2><?php } ?>
        <?php if(get_sub_field('body')){ ?> <div class="desc"><?php the_sub_field('body'); ?></div><?php } ?>
        <?php if(get_sub_field('link')){ 
                $link = get_sub_field('link');
        ?>
                <a href="<?php echo $link['url'];?>" class="btn"><?php echo $link['title'];?></a>
        <?php } ?>
    </div>
</div>