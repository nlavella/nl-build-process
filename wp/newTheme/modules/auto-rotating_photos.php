<div class="cont">              
    <?php if(get_sub_field('slides')){ ?>
    <div class="swiper-section inset" data-swiper>
        <div class="swiper-cont" data-desktop-slides="4">
            <div class="swiper-wrapper">
            <?php foreach(get_sub_field('slides') as $slide){ ?>
                <div class="swiper-slide">
                    <?php if($slide['link']){ echo '<a href="'.$slide['link']['url'].'" target="'.$slide['link']['target'].'">'; }?>
                    <div class="img-crop contain square">
                        <?php if($slide['image']){
                            $responsive_image = $slide['image'];
                            echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                        } ?>  
                    </div>
                    <?php if($slide['link']){ echo '</a>'; }?>
                </div>
            <?php } ?>
                </div><!-- /swiper-wrapper -->
            </div><!-- /swiper-cont -->
            <div class="swiper-button-prev" data-swiper-prev></div>
            <div class="swiper-button-next" data-swiper-next></div>
        </div><!-- /swiper-section -->
    <?php } ?>
</div>