<div class="cont">
    <div class="column-layout">
        <div class="col">
            <?php 
                $brand = get_field('brand');
                if($brand){ ?>
                    <?php if(get_field('logo', $brand[0]->ID)){
                        $responsive_image = get_field('logo', $brand[0]->ID);
                        echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => 'logo', 'alt' => $responsive_image['alt'] )); 
                    } ?>
            <?php } ?>
            <h1><?php the_sub_field('headline'); ?></h1>
            <?php if(get_sub_field('body')){ ?> <div class="desc"><?php the_sub_field('body'); ?></div><?php } ?>
            <?php 
                $button = get_sub_field('button'); 
                if($button['pdf_or_link'] == "pdf" && $button['file']){ ?>
                <a href="<?php echo $button['file']['url']; ?>" class="btn" target="_blank"><?php echo $button['title']; ?></a>
            <?php }else if($button['link']){ ?>
                <a href="<?php echo $button['link']['url']; ?>" class="btn" target="<?php echo $button['link']['target']; ?>"><?php echo $button['link']['title']; ?></a>
            <?php } ?>
        </div>
        <?php if(get_sub_field('image')){ ?>
        <div class="col">
            <div class="img-crop square">
                <?php 
                    $responsive_image = get_sub_field('image');
                    echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                ?>
            </div>
        </div>
        <?php } ?>
    </div>
</div>