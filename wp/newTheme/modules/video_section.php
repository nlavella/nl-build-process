<div class="cont">
    <?php if(get_sub_field('headline')){ ?><h2><?php the_sub_field('headline'); ?></h2><?php } ?>
    <?php if(get_sub_field('video_link')){ ?> <div class="responsive-video"><div class="video-size-frame"><?php the_sub_field('video_link'); ?></div></div><?php } ?>
</div>