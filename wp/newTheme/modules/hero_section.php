<div class="full" style="background-image:url(<?php $img = get_sub_field('image'); echo $img['url']; ?>);"> 
    <div class="cont">
        <?php if(get_sub_field('headline')){ ?><h1><?php the_sub_field('headline'); ?></h1><?php } ?>  
        <?php if(get_sub_field('subheadline')){ ?><p class="title2"><?php the_sub_field('subheadline'); ?></p><?php } ?> 
        <?php if(get_sub_field('link')){ $link = get_sub_field('link');?><a href="<?php echo $link['url']; ?>" class="btn center"><?php echo $link['title']; ?></a><?php } ?>  
    </div>
</div>