<div class="cont">
        <?php if(get_sub_field('headline')){ ?><h2><?php the_sub_field('headline'); ?></h2><?php } ?>
        <?php if(get_sub_field('subheadline')){ ?><p><?php the_sub_field('subheadline'); ?></p><?php } ?>
        <div class="flex-center">
        <?php $statistics = get_sub_field('statistics');
                
        foreach($statistics as $result){ ?>
                <div class="col result">
                        <div class="inner">
                                <p class="number title4"><span data-countup="<?php echo $result['number']; ?>"><?php echo '0';//echo $result['number']; ?></span><?php if($result['percent']){echo '%';} ?></p>
                                <p class="label"><?php echo $result['description']; ?></p>
                        </div>
                </div>
        <?php } ?>
        </div>
</div>