<div class="cont">
        <?php if(get_sub_field('headline')){ ?><h2><?php the_sub_field('headline'); ?></h2><?php } ?>
        <?php if(get_sub_field('subheadline')){ ?><p class="title"><?php the_sub_field('subheadline'); ?></p><?php } ?>    
        <div class="accordion-section" data-accordion="solo">
                <?php foreach(get_sub_field('accordions') as $acc){ ?>
                <div class="accordion-item" data-accordion-item>
                        <div class="accordion-header" data-accordion-header>
                                <div class="accordion-icon">
                                        <span data-accordion-open>+</span>
                                        <span data-accordion-close>-</span>
                                </div>
                                <p><?php echo $acc['title']; ?></p>
                        </div>
                        <div class="accordion-details">
                                <?php echo $acc['body']; ?>
                        </div>
                </div>
                <?php } ?>
        </div>
</div>