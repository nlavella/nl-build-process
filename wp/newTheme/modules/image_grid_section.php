<div class="cont">
    <?php if(get_sub_field('headline')){ ?><h2><?php the_sub_field('headline'); ?></h2><?php } ?>
    <div class="flex-center">
        <?php foreach(get_sub_field('images') as $item){
            echo '<div class="col">';
            echo wp_get_attachment_image( $item['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] ));
            echo '</div>';
        } ?>
    </div>
</div>