<div class="cont">
    <div class="column-layout">
        <div class="col">
        <?php   $media = get_sub_field("media");
                if($media['images_or_video'] === 'video'){ ?>
                    <div class="responsive-video">
                            <div class="video-size-frame"><?php echo $media['video_embed_code']; ?></div>
                    </div>
            <?php }else{ ?>
                    <?php if(count($media['images']) == 1){ 
                            foreach($media['images']  as $image){ ?>
                                <div class="img-crop sixteen-nine">
                                    <?php 
                                        $responsive_image = $image;
                                        echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                    ?>
                                </div>
                        <?php }
                            }else{ ?>
                            <div class="swiper-section btm-btns" data-swiper>
                                <div class="swiper-cont">
                                    <div class="swiper-wrapper">
                                    <?php foreach($media['images'] as $image){ ?>
                                        <div class="swiper-slide">
                                            <div class="img-crop sixteen-nine">
                                                <?php 
                                                    $responsive_image = $image;
                                                    echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                                ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    </div><!-- /swiper-wrapper -->
                                </div><!-- /swiper-cont -->
                                <div class="swiper-button-prev" data-swiper-prev></div>
                                <div class="swiper-button-next" data-swiper-next></div>
                            </div><!-- /swiper-section -->
                        <?php 
                            } ?>
            <?php } ?>
        </div>
        <div class="col">
            <?php if(get_sub_field('headline')){ ?><h2><?php the_sub_field('headline'); ?></h2><?php } ?>
            <?php if(get_sub_field('body')){ ?> <div class="desc"><?php the_sub_field('body'); ?></div><?php } ?>
        </div>
    </div>
</div>