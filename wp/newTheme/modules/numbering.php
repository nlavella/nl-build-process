<div class="cont">    
    <?php if(get_sub_field('headline')){ ?><h2 class="title1"><?php the_sub_field('headline'); ?></h2> <?php } ?>
        <?php $count = 1; foreach(get_sub_field('steps') as $step){ ?>
            <div class="column-layout">
                <?php if($step['image']){ ?>
                <div class="col">
                    <div class="img-crop square">
                        <?php 
                            $responsive_image = $step['image'];
                            echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                        ?>
                    </div>
                </div>
                <?php } ?>
                <div class="col">
                    <p class="count"><?php echo $count; ?></p>
                    <?php if($step['title']){ ?><p class="title4"><?php echo $step['title']; ?></p><?php } ?>
                    <?php if($step['description']){ ?> <div class="desc"><?php echo $step['description']; ?></div><?php } ?>
                </div>     
            </div>
        <?php $count ++; } ?> 
</div>
