<div class="full">
        <div class="cont">
                <?php if(get_sub_field('headline')){ ?><h2><?php the_sub_field('headline'); ?></h2><?php } ?>
                <?php if(get_sub_field('subheadline')){ ?><p class="title"><?php the_sub_field('subheadline'); ?></p><?php } ?>
                <?php if(get_sub_field('body')){ ?> <div class="desc"><?php the_sub_field('body'); ?></div><?php } ?>
        </div>
</div>

<?php if(get_sub_field('sections')){ ?>
        <?php foreach(get_sub_field('sections') as $section){ ?>
            <div class="general_section">
                <div class="cont">
                        <div class="flex-center">
                                <div class="col">
                                <?php if($section['image']){
                                        $responsive_image = $section['image'];
                                        echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                                } ?>
                                </div>
                                <div class="col">
                                <?php if($section['headline']){ ?><h3><?php echo $section['headline']; ?></h3><?php } ?>
                                <?php if($section['body']){ ?> <div class="desc"><?php echo $section['body']; ?></div><?php } ?>
                                <?php if($section['link']){ 
                                        $link = $section['link'];
                                ?>
                                        <a href="<?php echo $link['url'];?>" class="btn"><?php echo $link['title'];?></a>
                                <?php } ?>
                                </div>
                        </div>
                </div> 
            </div>
        <?php } ?>
<?php } ?>