<div class="cont">
    <div class="flex-center">
        <?php if(get_sub_field('headline')){ ?><p class="title1"><?php the_sub_field('headline'); ?></p><?php } ?>
        <?php if(get_sub_field('button_text')){ ?><a data-fancybox href="#contact-modal" class="btn secondary"><?php the_sub_field('button_text'); ?></a><?php } ?>
    </div>
</div>