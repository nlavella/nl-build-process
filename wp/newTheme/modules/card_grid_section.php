<div class="cont">
    <?php if(get_sub_field('headline')){ ?><h2><?php the_sub_field('headline'); ?></h2><?php } ?>
    
    <?php if(get_sub_field('cards')){ ?>
        <div class="card-layout thirds">
        <?php foreach(get_sub_field('cards') as $section){ ?>
            <div class="card">
                <?php if($section['image']){
                    $responsive_image = $section['image'];
                    echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                } ?>
                <?php if($section['headline']){ ?><h3><?php echo $section['headline']; ?></h3><?php } ?>
                <?php if($section['body']){ ?> <div class="desc"><?php echo $section['body']; ?></div><?php } ?>
                <?php if($section['link']){ 
                    $link = $section['link'];
                ?>
                    <a href="<?php echo $link['url'];?>" class="btn"><?php echo $link['title'];?></a>
                <?php } ?>
            </div>
        <?php } ?>
        </div>
    <?php } ?>
</div>