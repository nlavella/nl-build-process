<?php
/**
 * Single Post
**/

get_header(); ?>

		<?php if ( have_posts() ) :
			while ( have_posts() ) : the_post(); 
		?>

			<section class="module focus hero_section">
				<div class="full" style="background-image:url(<?php $hero = get_field('hero'); echo $hero['image']['url']; ?>);"> 
					<div class="cont">
						<h1><?php echo the_title(); ?></h1>
						<p class="title3"><?php echo the_date(); ?></p>
					</div>
					
					<?php get_template_part('accent'); ?>
				</div>
			</section>
				
		<?php endwhile; 
		endif; ?>

<?php if(have_rows('content_sections')){

while (have_rows('content_sections')) : the_row();

	include ( locate_template('module_base.php', false, false));

endwhile;

}
?>
<?php /*
<div class="related">
	<div class="cont">
		<?php
			$args = array(	'post_type' => 'post',
							'posts_per_page' => 15,
							'post__not_in' => array($post->ID)
						);
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post();
		?> 
				<a href="<?php echo the_permalink();?>"><?php echo the_title(); ?></a>
		<?php	
			endwhile;
		?>
	</div>
</div>
*/ ?>

<?php get_footer(); ?>
