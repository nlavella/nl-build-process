<?php
/**
 * Blog Index
 */

get_header(); ?>
<section class="module focus hero_section">
	<div class="full" style="background-image:url(<?php $hero = get_field('blog_hero', 'options'); echo $hero['url']; ?>);"> 
		<div class="cont">
			<h1>Blog</h1>
		</div>
		
		<?php get_template_part('accent'); ?>
	</div>
</section>

<div class="full blog-posts">
	<div class="cont">
		<?php if ( have_posts() ) : ?>

			<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();
			?>
				<div class="blog-entry">
					<a href="<?php echo the_permalink();?>" class="thumbnail">
						<?php if(get_field('thumbnail')){
                            $responsive_image = get_field('thumbnail');
                            echo wp_get_attachment_image( $responsive_image['id'], 'full', false, array( 'class' => '', 'alt' => $responsive_image['alt'] )); 
                        } ?>
					</a>
					<div class="inner">
						<a href="<?php echo the_permalink();?>"><p class="title3"><?php echo the_title(); ?></p></a>
						<p><?php the_field('preview'); ?></p>
						<a href="<?php echo the_permalink();?>" class="btn">Read more</a>
					</div>
				</div>
			<?php 
			// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'PREV', '' ),
				'next_text'          => __( 'NEXT', '' ),
			) );

		endif;
		?>
	</div>
</div>

<?php get_footer(); ?>
