<?php
/**
 * 404
 */

get_header(); ?>

<section class="full 404">
	<div class="cont">
		<h1>Page not found.</h1>
	</div>
</section>

<?php get_footer(); ?>
