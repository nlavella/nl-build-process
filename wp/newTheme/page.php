<?php
/**
 * Page Builder
 */

get_header(); ?>

<?php if(have_rows('content_sections')){

		while (have_rows('content_sections')) : the_row();

			include ( locate_template('module_base.php', false, false));
		
		endwhile;

	}
?>

<?php get_footer(); ?>
