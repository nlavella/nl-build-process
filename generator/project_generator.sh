#!/bin/sh
# grab all the module names and build out starter sass files 
INPUT=modules.csv
OLDIFS=$IFS
IFS=","
[! -f $INPUT] && { echo "$INPUT not found"; exit 99;}

while read ModuleID ModuleName
do 
    touch $ModuleID-$ModuleName.scss
    echo ".$ModuleID-$ModuleName{"      >> $ModuleID-$ModuleName.scss
    echo "@include paddingTopBottom;"   >> $ModuleID-$ModuleName.scss
    echo "}"                            >> $ModuleID-$ModuleName.scss
done < $INPUT

# create main + add all sass imports
touch main.scss
while read ModuleID ModuleName
do 
    echo "@import \"modules/$ModuleID-$ModuleName\";" >> main.scss
done < $INPUT

# create all html frames
while read ModuleID ModuleName
do 
    touch $ModuleID-$ModuleName.html
    echo "<section class=\"$ModuleID-$ModuleName to-do\">"                  >> $ModuleID-$ModuleName.html
	echo "<div class=\"debug-title\">$ModuleID ${ModuleName//-/ }</div>"    >> $ModuleID-$ModuleName.html
	echo "<div class=\"container\">"                                        >> $ModuleID-$ModuleName.html
	echo "</div>"                                                           >> $ModuleID-$ModuleName.html
    echo "</section>"                                                       >> $ModuleID-$ModuleName.html
done < $INPUT

# create main + add all html imports
touch main.html
while read ModuleID ModuleName
do 
    echo "{% include '../modules/$ModuleID-$ModuleName.html' %}" >> main.html
done < $INPUT

IFS=$OLDIFS
