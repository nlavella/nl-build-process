/*
 * ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 * ========================================================================
 */

// Use this variable to set up the common and page specific functions. If you
// rename this variable, you will also need to rename the namespace below.

var NL = {
	// All pages
	common: {
		init: function () {
			// NAV FUNCTION INIT
			NL.common.navigation();

			// Sticky Nav
			NL.common.stickyNav();

			// Scroll links
			NL.common.scrollLinks();

			// Tab Sections
			tabSections();

			// accordion Section
			accordionSections();

			//setup swipers
			initSwipers();

			// Scroll Focus
			scrollFocus();

			// Scroll Animation
			// scrollAnimation();

			// Parallax Animation
			// parallaxAnimation();

			// Chase Animation
			// chaseAnimation();

			// Gallery Sections
			//gallerySection();

			// Infinite Rotate
			// see _infiniteRotate.js

			// counting numbers
			initCountUp();

			// SJ Slides
			//initSJSlides();
		},

		/*
		 * ========================================================================
		 * Navigation collapse & expand functionality
		 * ========================================================================
		 */
		navigation: function () {
			//mobile menu toggle
			$(".menu-toggle").click(function () {
				$("nav").toggleClass("active");
				$("header").toggleClass("active");
			});
		},

		stickyNav: function () {
			var scrollPos = 0;

			//scroll functions
			$(window).scroll(function () {
				if (window.innerWidth >= 960) {
					//960
					//add scroll class to header
					if ($(this).scrollTop() > 80) {
						if (document.body.getBoundingClientRect().top > scrollPos) {
							//up
							// this refers to window
							$("header").addClass("scroll");
							$("body").addClass("scroll");
							$(".cta-bar").addClass("active");
						} else {
							//down
							$("header").removeClass("scroll");
							$("body").removeClass("scroll");
							$(".cta-bar").removeClass("active");
						}
					} else {
						$("header").removeClass("scroll");
						$("body").removeClass("scroll");
						$(".cta-bar").removeClass("active");
					}
				}

				// saves the new position for iteration.
				scrollPos = document.body.getBoundingClientRect().top;
			});
		},

		scrollLinks: function () {
			$(".scroll-link").click(function (event) {
				$(".scroll-link").removeClass("active");
				$(this).addClass("active");
				event.preventDefault();

				$("html,body").animate(
					{
						scrollTop: $(this.hash).offset().top - 50,
					},
					1000
				);

				if (window.innerWidth <= 768) {
					$("nav").removeClass("active");
				}
			});
		},
	},

	// page specific start

	homepage: {
		init: function () {},
	},
	// page specific end
};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var NL_UTIL = {
	fire: function (func, funcname, args) {
		var fire;
		var namespace = NL;
		funcname = funcname === undefined ? "init" : funcname;
		fire = func !== "";
		fire = fire && namespace[func];
		fire = fire && typeof namespace[func][funcname] === "function";

		if (fire) {
			namespace[func][funcname](args);
		}
	},
	loadEvents: function () {
		// Fire common init JS
		NL_UTIL.fire("common");

		document.body.className
			.replace(/-/g, "_")
			.split(/\s+/)
			.forEach(function (classnm) {
				NL_UTIL.fire(classnm);
				NL_UTIL.fire(classnm, "finalize");
			});
	},
};

// Load Events
NL_UTIL.loadEvents();
