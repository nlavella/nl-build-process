//check all elements that should activate on scrolls
/* example structure */
//<section class="scroll-animation" data-scroll-depth="70" id="#"></section>

function scrollAnimation() {
	$(".scroll-animation").each(function () {
		let depth = 50;
		if ($(this).attr("data-scroll-depth")) {
			depth = $(this).attr("data-scroll-depth");
		}
		//if scrolled into view and not already activated
		if (isScrolledIntoView(this, depth) && !$(this).hasClass("active")) {
			//mark as activated
			$(this).addClass("active");
		}
	});

	$(window).scroll(function () {
		//check all elements that should activate on scrolls
		/* example structure */
		//<section class="scroll-animation" data-scroll-depth="70" id="#"></section>
		$(".scroll-animation").each(function () {
			let depth = 50;
			if ($(this).attr("data-scroll-depth")) {
				depth = $(this).attr("data-scroll-depth");
			}

			//if scrolled into view and not already activated
			if (isScrolledIntoView(this, depth) && !$(this).hasClass("active")) {
				//mark as activated
				$(this).addClass("active");
			}
		});
	});
}
