var currentFocus;

function scrollFocus() {
	setTimeout(function () {
		focus();
	}, 2000);

	$(window).scroll(function () {
		//check all elements that should activate on scrolls
		focus();
	});

	function focus() {
		var currentlyAbove = new Array();
		$(".focus").each(function () {
			//if scrolled into view and not already activated
			if ($(this).hasClass("cta_section") && isScrolledIntoView(this, 25)) {
				currentlyAbove.push(this);
				$(this).addClass("viewed");
			} else if (passTheHalf(this)) {
				currentlyAbove.push(this);
				$(this).addClass("viewed");
			}
		});

		if (currentlyAbove[currentlyAbove.length - 1] !== currentFocus) {
			//mark as activated
			$(".focus").removeClass("inFocus");
			$(currentlyAbove[currentlyAbove.length - 1]).addClass("inFocus");
			currentFocus = currentlyAbove[currentlyAbove.length - 1];

			//mark nav
			$("a.focus-link").each(function () {
				var currLink = $(this);
				var refElement = $(currLink.attr("href"));
				console.log(refElement);
				console.log(currentFocus);
				if ($(refElement).attr("id") === $(currentFocus).attr("id")) {
					$("a.focus-link").removeClass("active");
					currLink.addClass("active");
				}
			});
		}
	}
}

function bottomSection(elem) {
	$(window).scroll(function () {
		var viewTop = $(window).scrollTop();
		var viewBottom = viewTop + $(window).height();
		var eleTop = $(elem).offset().top;

		if (eleTop <= viewBottom) {
			//console.log("contact");
			currentFocus = "contact";
			$(".focus").removeClass("inFocus");
			$("a.focus-link").removeClass("active");
			$("a.focus-link[href$='#contact']").addClass("active");
		}
	});
}
