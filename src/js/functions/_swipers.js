function initSwipers() {
	console.log("setup sliders");
	// grab all slider modules
	const swiperSections = document.querySelectorAll("[data-swiper]");

	for (var i = 0; i < swiperSections.length; i++) {
		let _swiperSection = swiperSections[i];

		if (!_swiperSection.classList.contains("swiper-cont")) {
			_swiperSection = swiperSections[i].querySelector(".swiper-cont");
		}

		let _swiperPrev = swiperSections[i].querySelector("[data-swiper-prev]");
		let _swiperNext = swiperSections[i].querySelector("[data-swiper-next]");
		let _swiperPagination = swiperSections[i].querySelector("[data-swiper-pagination]");
		let _swiperLoop = false;

		if (_swiperSection.dataset.loop === "true") {
			_swiperLoop = true;
		}

		let _slidesPerView = 1;
		let _slidesPerViewTablet = 1;
		let _spaceBetween = 0;
		let _spaceBetweenTablet = 0;
		if (_swiperSection.dataset.desktopSlides) {
			_slidesPerView = _swiperSection.dataset.desktopSlides;
			_spaceBetween = 30;
		}

		if (_swiperSection.dataset.tabletSlides) {
			_slidesPerViewTablet = _swiperSection.dataset.tabletSlides;
			_spaceBetweenTablet = 15;
		}

		//swiper
		var _swiper = new Swiper(_swiperSection, {
			loop: _swiperLoop,
			slidesPerView: _slidesPerView,
			spaceBetween: _spaceBetween,
			pagination: {
				el: _swiperPagination,
				clickable: true,
			},
			autoplay: {
				delay: 3500,
			},
			navigation: {
				nextEl: _swiperNext,
				prevEl: _swiperPrev,
			},
			breakpoints: {
				1400: {
					slidesPerView: _slidesPerViewTablet,
					spaceBetween: _spaceBetweenTablet,
				},
				768: {
					slidesPerView: 1,
					spaceBetween: 0,
				},
			},
		});

		/* thumbnail nav 
    swiper.on("slideChange", function () {
      $(".swiper-link").removeClass("active");
      $(
        ".swiper-link[data-slide-target='" + residenceSlider.activeIndex + "']"
      ).addClass("active");
    });

    $(".swiper-link").click(function () {
      _swiper.slideTo($(this).attr("data-slide-target"));
    });
    */

		//swipers.push(_swiper);
	}
}
