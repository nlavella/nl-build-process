//check all elements that should activate on scroll
/* example structure */
//<div class="chase" data-chase-speed="650" data-scroll-depth="80"><div class="chase-element" id="#"></div>

function chaseAnimation() {
  $(".chase").each(function () {
    //if scrolled into view and not already activated
    if (
      isScrolledIntoView(this, $(this).attr("data-scroll-depth")) &&
      !$(this).hasClass("active")
    ) {
      //mark as activated
      $(this).addClass("active");
      chase(this, $(this).attr("data-chase-speed"));
    }
  });

  $(window).scroll(function () {
    $(".chase").each(function () {
      //if scrolled into view and not already activated
      if (
        isScrolledIntoView(this, $(this).attr("data-scroll-depth")) &&
        !$(this).hasClass("active")
      ) {
        //mark as activated
        $(this).addClass("active");
        chase(this, $(this).attr("data-chase-speed"));
      }
    });
  });
}

//chase
function chase(element, duration) {
  var inc = 0;

  $(element)
    .find(".chase-element")
    .each(function (index, element) {
      var chase_element = $(this);

      setTimeout(function () {
        chase_element.addClass("active");
      }, duration * inc);

      inc++;
    });
}
