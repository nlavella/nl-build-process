// Tab section functionality
function tabSections() {
  console.log("setup tabs");
  // grab all tab modules
  const tabSections = document.querySelectorAll("[data-tab-section]");

  for (var i = 0; i < tabSections.length; i++) {
    // grab the links
    const tabLinks = tabSections[i].querySelectorAll("[data-tab-target]");

    // grab the tabs
    const tabConts = tabSections[i].querySelectorAll("[data-tab]");

    for (var j = 0; j < tabLinks.length; j++) {
      let _tabSection = tabSections[i];

      tabLinks[j].addEventListener("click", function () {
        // clear active tabs
        removeClassAll(tabLinks, "active");
        removeClassAll(tabLinks, "mobile-active");
        removeClassAll(tabConts, "active");
        removeClassAll(tabConts, "mobile-active");

        // add active to selected
        this.classList.add("active");
        _tabSection
          .querySelector("[data-tab='" + this.dataset.tabTarget + "']")
          .classList.add("active");
        _tabSection.querySelector(".tab-cont").classList.add("active");
      });
    }
  }
}
