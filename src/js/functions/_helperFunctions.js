// is an element above percentage
function isScrolledIntoView(elem, percent) {
	//viewport height
	var viewTop = $(window).scrollTop();
	var viewBottom = viewTop + $(window).height() - $(".quote-bar").outerHeight();

	//% of element is showing to trigger
	var elemTrigger =
		$(elem).offset().top + $(elem).outerHeight() * (percent / 100);
	var amountOverTrigger = elemTrigger - viewBottom;

	/*
	console.log(elem);
	console.log(elem.getBoundingClientRect());
	console.log($(elem).offset().top + "," + $(elem).outerHeight());
	console.log(elemHalf + "," + viewBottom);
	console.log(viewTop + "," + viewBottom);
	console.log("Amount over marker: " + ());

	console.log($(".quote-bar").outerHeight());

	var pixelsShowing = viewBottom - elem.getBoundingClientRect().top;
	console.log("PIXELS SHOWING: " + pixelsShowing);
	console.log(
		"PERCENT SHOWING: " + pixelsShowing / elem.getBoundingClientRect().height
	);
*/

	if (window.innerWidth < 768) {
		//elemHalf = $(elem).offset().top + 1 * ($(elem).outerHeight() / 2);
	}

	return amountOverTrigger < 0;
}

// has an element passed the half
function passTheHalf(elem) {
	//viewport height
	var viewTop = $(window).scrollTop();
	var viewBottom = viewTop + $(window).height() * 0.5;

	//half of element is showing then return
	var eleTop = $(elem).offset().top;

	//console.log(elem);
	//console.log(elemHalf + "," + viewBottom);

	return eleTop <= viewBottom;
}

// Remove class from all elements
function removeClassAll(elements, className) {
	for (var i = 0; i < elements.length; i++) {
		elements[i].classList.remove(className);
	}
}

// Remove class from all elements by selector
function removeClassAllSelector(selector, className) {
	let elements = document.querySelectorAll(selector);
	for (var i = 0; i < elements.length; i++) {
		elements[i].classList.remove(className);
	}
}

// Add class from element by selector
function addClass(selector, className) {
	let element = document.querySelector(selector);
	element.classList.add(className);
}

// Remove class from element by selector
function removeClass(selector, className) {
	let element = document.querySelector(selector);
	element.classList.remove(className);
}
