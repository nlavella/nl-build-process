// accordion section functionality
function accordionSections() {
	console.log("setup accordions");
	// grab all tab modules
	const accordionSections = document.querySelectorAll("[data-accordion]");

	for (var i = 0; i < accordionSections.length; i++) {
		// solo setting
		let _solo = accordionSections[i].dataset.accordion === "solo";
		console.log(_solo);

		// grab the items
		const accordionItems = accordionSections[i].querySelectorAll(
			"[data-accordion-item]"
		);

		for (var j = 0; j < accordionItems.length; j++) {
			let _accordionSection = accordionItems[j];
			let _accordionHeader = accordionItems[j].querySelector(
				"[data-accordion-header]"
			);

			_accordionHeader.addEventListener("click", function () {
				let _isOpen = _accordionSection.classList.contains("open");

				if (_solo) {
					// clear active tabs
					removeClassAll(accordionItems, "open");
				} else {
					// clear open tab
					_accordionSection.classList.remove("open");
				}

				if (!_isOpen) {
					// add active to selected
					_accordionSection.classList.add("open");
				}
			});
		}
	}
}
